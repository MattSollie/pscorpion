﻿Function ConvertTo-HashTable {
    <#
    .Synopsis
        Convert an object to a HashTable
    .Description
        Convert an object to a HashTable excluding certain types.  For example, ListDictionaryInternal doesn't support serialization therefore
        can't be converted to JSON.
    .Parameter InputObject
        Object to convert
    .Parameter ExcludeTypeName
        Array of types to skip adding to resulting HashTable.  Default is to skip ListDictionaryInternal and Object arrays.
    .Parameter MaxDepth
        Maximum depth of embedded objects to convert.  Default is 4.
    .Example
        $bios = get-ciminstance win32_bios
        $bios | ConvertTo-HashTable
    #>
    
    Param (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)]
        [Object]$InputObject,
        [string[]]$ExcludeTypeName = @("ListDictionaryInternal","Object[]"),
        [ValidateRange(1,10)][Int]$MaxDepth = 4
    )

    Process {

        Write-Verbose "Converting to hashtable $($InputObject.GetType())"
        #$propNames = Get-Member -MemberType Properties -InputObject $InputObject | Select-Object -ExpandProperty Name
        $propNames = $InputObject.psobject.Properties | Select-Object -ExpandProperty Name
        $hash = @{}
        $propNames | ForEach-Object {
            if ($InputObject.$_ -ne $null) {
                if ($InputObject.$_ -is [string] -or (Get-Member -MemberType Properties -InputObject ($InputObject.$_) ).Count -eq 0) {
                    $hash.Add($_,$InputObject.$_)
                } 
                else {
                    if ($InputObject.$_.GetType().Name -in $ExcludeTypeName) {
                        Write-Verbose "Skipped $_"
                    } 
                    elseif ($MaxDepth -gt 1) {
                        $hash.Add($_,(ConvertTo-HashTable -InputObject $InputObject.$_ -MaxDepth ($MaxDepth - 1)))
                    }
                }
            }
        }
        $hash
    }
}
<#
.Synopsis
   Create a new Route File for a PScorpion Server
.DESCRIPTION
   Create a new Route File for a PScorpion Server
.EXAMPLE
   New-PScorpionRouteFile MyRoutes
#>
function New-PScorpionRouteFile {
    [CmdletBinding()]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$true,
                   Position=0)]
        $Name
    )

    Begin
    {
$route = @'
#Route Names can be a simple string, or use a regex to validate the URL
#   Example: "userid" or "^userid/\d{10}$
#Match groupings are also supported such as the route: "^route/(?<user>[A-Z0-9.-]+)/(?<prop>[A-Z0-9.-]+)/?$" = @{
#   Example: /route/bob.smith/FullName  would give $Matches.user and $matches.prop
#
#Data Available within these routes:
# $data: a hash of all query string Data, accessible as $data["key"]
#   Example: /route?id=1234&user=bob
#   accessible as $data['id'] and $data['user']
# $SubUrl: Remainder of route after the initial route
#   Example: /route/web/page.html
#   $SubUrl = web/page.html
# $UrlParts: Array of URL elements
#   Example: /route/bob/profile:
#   $UrlParts = @(route, bob, profile); $urlparts[1] = bob
@{
    #"routename" = @{
    #	info = "description"
    #	security = "allusers <or> AD_Group_Name"
    #   init = {"commands to run at startup"}
    #	get = {}
    #	post = {}
    # };
    #Enable Static Content:
    "^pages/.+$" = @{
	    info = "get static content"
        security = "allusers"
        get = {get-content .\static\$SubUrl -raw}
        post = {}
	};
    #Enable Dynamic Reload of Routes:
    "^reloadroutes$" = @{
	    info = "Reload Routes on POST"
            security = "automationservices"
            get = {}
            post = {$routes = . $routefile; Write-Verbose $routes}
	};
    "info" = @{
	    info = "Return All Routes"
	    security = "allusers"
	    get = {
		    foreach ($r in $routes.keys) {"$r`: $($routes[$r].info)"}
	    }
	    post = {}
    }
}
'@
    }
    Process
    {
        $route | out-file "$Name.ps1"
    }
    End
    {
    }
}
<#
.Synopsis
   Create a new server folder and configuration
.DESCRIPTION
   Creates a server folder in the current location with default settings, or specify other options
.EXAMPLE
   New-PscorpionServer -Port 8000
.EXAMPLE
   New-PscorpionServer C:\web\
#>
function New-PscorpionServer
{
    [CmdletBinding()]
    Param
    (
       #Name of Server instance
       [Parameter(Mandatory=$true,
                   ValueFromPipelineByPropertyName=$false,
                   Position=0)]        
        [String]$Name,
        # Path to service location
        [String]$path=".\",	
        [Parameter()]
        [Int] $Port = 5000,
        [Parameter()]
        [String] $ImportModule = "",		
		[Parameter()]
        [boolean] $CORS = $true,            
        [Parameter()]
        [System.Net.AuthenticationSchemes] $Auth = "NTLM"
    )

    Begin
    {
    }
    Process
    {
        $BasePath = Get-Item $path
        $ServerBase = New-Item -ItemType Directory -Path $BasePath -Name $Name
        Set-Location $ServerBase
        New-Item -ItemType Directory -Path $ServerBase -Name Static  |Out-Null
        New-PScorpionRouteFile -Name "$($Name)_Routes"
        @{ServerName=$Name;RouteFile=".\$($Name)_Routes.ps1";Port=$Port;CORS=$CORS;UrlValidation=$UrlValidation;Auth=$Auth} | Export-Clixml config.xml

    }
    End
    {
    }
}

<#
.Synopsis
   Start Server as Job
.DESCRIPTION
   Start server as PS Job
#>
function Start-PSCorpionJob
{
    [CmdletBinding()]
    [Alias()]
    [OutputType([int])]
    Param
    (
        [Parameter(Mandatory = $false, Position=0)]
        [ValidateScript({Test-Path $_ -Include *.xml})]                                      
        [string] $ConfigFile=".\config.xml"
        )

    Begin
    {
    }
    Process
    {
        $config = Import-Clixml $ConfigFile
        Write-Information "Starting Server Job $($config.ServerName) with command `"Start-Pscorpion -ConfigFile $((Get-Item $configFile).FullName)`""
        Start-Job -Name $config.ServerName -ScriptBlock {Start-Pscorpion -ConfigFile $args[0] -Verbose} -ArgumentList (Get-Item $configFile).FullName
    }
    End
    {
    }
}

Function Start-Pscorpion {
    <#
    .Synopsis
        Creates a new HTTP Listener accepting PowerShell command line to execute
    .Description
        Creates a new HTTP Listener enabling a remote client to execute PowerShell command lines using a simple REST API.
        This function requires running from an elevated administrator prompt to open a port.

        Use Ctrl-C to stop the listener.  You'll need to send another web request to allow the listener to stop since
        it will be blocked waiting for a request.
    .Parameter Port
        Port to listen, default is 8888
    .Parameter URL
        URL to listen, default is /
    .Parameter ImportModule
        Module to Import to support BaseCommand
    .Parameter BaseCommand
        Base command to pass the parameter to
    .Parameter Auth
        Authentication Schemes to use, default is IntegratedWindowsAuthentication
    .Example
        Start-HTTPListener -Port 8080 -Url PowerShell -ImportModule ExampleModule -BaseCommand Get-Service
        Invoke-WebRequest -Uri "http://localhost:8888/PowerShell?command=winmgmt&format=text" -UseDefaultCredentials | Format-List *
    #>
    [CmdletBinding(DefaultParameterSetName="InConfig")]
    Param (
        [Parameter(Mandatory = $false,
                    ValueFromPipelineByPropertyName=$true,
                   Position=0, ParameterSetName = "InConfig" )]
        [ValidateScript({Test-Path $_ -Include *.xml})]                                      
        [string] $ConfigFile=".\config.xml",

        [Parameter(Mandatory = $true,
                    ValueFromPipelineByPropertyName=$true,
                   Position=0, ParameterSetName = "InCommand" )]
        [ValidateScript({Test-Path $_ -Include *.ps1})]
        [string] $RouteFile,

        [Parameter(ParameterSetName = "InCommand")]
        [Int] $Port = 8888,

        [Parameter(ParameterSetName = "InCommand")]
        [String] $ImportModule = "",
		
		[Parameter(ParameterSetName = "InCommand")]
        [switch] $CORS,
        
        [Parameter(ParameterSetName = "InCommand")]
        [System.Net.AuthenticationSchemes] $Auth = "NTLM"
        )
    Begin {
        switch ($PsCmdlet.ParameterSetName) {
            "InConfig" {
                try {
                    Set-Location (Get-Item $configFile).DirectoryName
                }
                catch {
                    break
                }
                $config = Import-Clixml $ConfigFile
                $Port = $config.port
                $ImportModule = $config.ImportModule
                $CORS = $config.CORS
                $Auth = $config.Auth
                $RouteFile = $config.RouteFile 
                $ServerName = $config.ServerName
            }
            "InCommand" {
                $ServerName = "PScorpion Server"
                try {
                    Set-Location (Get-Item $RouteFile).DirectoryName
                }
                catch {
                    break
                }
            }
        }
        $ErrorActionPreference = "Stop"

        #Check if Administrator
        $CurrentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent())
        if ( -not ($currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator ))) {
            Write-Error "This script must be executed from an elevated PowerShell session" -ErrorAction Stop
        }
		
        #Import Specified Modules
        if ($ImportModule) {
			Import-Module $ImportModule
        }

        #Load Routes
		$routes = . $routefile
        write-verbose "Loaded Routes: $($routes.keys)"
		
        #Run Initial commands
        foreach ($init in $routes.values.init) { . $init  }

        $prefix = "http://*:$Port/"
        $listener = New-Object System.Net.HttpListener        
        $listener.Prefixes.Add($prefix)
        $listener.AuthenticationSchemes = $Auth 
		Write-Warning "Note that thread is blocked waiting for a request.  After using Ctrl-C to stop listening, you need to send a valid HTTP request to stop the listener cleanly."
        Write-Warning "Sending 'exit' command will cause listener to stop immediately"
        Write-Host "$ServerName Listening on $port with authentication mode: $Auth"

    }

    Process {
        
        try {
            $listener.Start()
            while ($true) {
                try {
                    $statusCode = 200
                    $context = $listener.GetContext()
                    $request = $context.Request

                    $identity = $context.User.Identity					
                    $request | Format-List * | Out-String | Write-Debug                
                    $Format = $request.QueryString.Item("format")                   
                    $RouteName = $Request.Url.LocalPath.split("/")[1]
                    $Route = ($routes.GetEnumerator() | Where-Object {$Request.Url.LocalPath.substring(1) -match $_.Key}).value
              
				    if($route) {                         
					    #$Route = $routes[$RouteName]
					    Write-Verbose "Matched route $RouteName"
                        #Build Request Data
                        $SubURL = $Request.Url.LocalPath.split("/",3)[2]
                        $UrlParts =  $($Request.Url.LocalPath.substring(1)).split("/")
                        
					    if ($request.HttpMethod -eq 'POST') {
						    $srd = new-object System.IO.StreamReader $request.InputStream
						    $raw = $srd.ReadToEnd()	 
							$data = $raw| convertfrom-json	
					    }
					    elseif ($request.HttpMethod -eq 'GET') {							
						    $data = $Request.QueryString

					    }
                        else {
                        #No Data for other methods
                        }

                        #Run Route Command
		                #All Users Route
					    if ($Route.security -eq "allusers") {
                                try {     
                                    
								    $commandOutput = . $Route.$($request.HttpMethod)                                  					
									    
							    }
							    catch {
                                    Write-Verbose "Failed to run the route command"
                                    write-Verbose $_.Exception
                                    $statusCode = 404
								    $commandOutput = [string]::Empty
							    }
					    }
                        #Group Membership Routes
					    else {
						    if ($($(Get-ADGroupMember $Route.security).samaccountname).contains($($identity.Name.split("\")[1]))) {
							    try {
								    $commandOutput = . $Route.$($request.HttpMethod)
								    }
							    catch {
								    Write-Verbose "Failed to run the route command"
                                    wwrite-Verbose $_.Exception
                                    $statusCode = 404
								    $commandOutput = [string]::Empty
							    }
						    } 
                            else {
																
							    Write-Warning "Rejected request as user doesn't match current security principal of listener"
							    $statusCode = 403
							    $commandOutput = "Unauthorized"
						    }
					    }	
				    } 
				    else {
					    Write-Verbose "No route found for $($Request.Url)"
                        $StatusCode = 400
				    }
                    
                    #Format Output
                    if (($RouteName -eq 'pages') -or ($RouteName -eq 'templates')) {
                        $Format = "text/plain"
                    }
                      elseif ($Null -eq $Request.ContentType) {
                        $Format = "application/json"
                    }
                    else {
                        $Format = $request.ContentType
                    }
             
                    
				    if ($commandOutput) { 
					    $commandOutput = switch ($Format) {
						    "text/plain"    { $commandOutput | Out-String ; break } 
						    "application/json"    { $commandOutput | ConvertTo-JSON -Depth 5 ; break }
						    "application/xml"     { $commandOutput | ConvertTo-XML -As String; break }
						    "application/clixml"  { [System.Management.Automation.PSSerializer]::Serialize($commandOutput) ; break }
						    default { $commandOutput | ConvertTo-JSON -Depth 5 ; break }
					    }
				    }

                    Write-Verbose "$(get-date -format g): $($request.UserHostAddress) : $($context.User.Identity.Name) : $( $request.URL) : $statusCode "							
				    $response = $context.Response
                    $response.StatusCode = $statusCode
                    if ($CORS) {
					    if (($request.HttpMethod -eq "OPTIONS") -or  ($request.HttpMethod -eq "GET")) {
								    $response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With");
								    $response.AddHeader("Access-Control-Allow-Methods", "GET, POST");
								    $response.AddHeader("Access-Control-Max-Age", "1728000");
								    $response.AddHeader("Access-Control-Allow-Credentials"," true")
						    } 
				       if ($request.UrlReferrer) {
						    $response.AppendHeader("Access-Control-Allow-Origin", "http://$($request.UrlReferrer.authority)")
						    }
				       else {
						    $response.AppendHeader("Access-Control-Allow-Origin", "*")
						    }
				    }
				    if ($commandOutput -eq $false) {$commandOutput = "false"}
                    if (!$commandOutput) {
                        $commandOutput = [string]::Empty
                    }				
                    $buffer = [System.Text.Encoding]::UTF8.GetBytes($commandOutput)

                    $response.ContentLength64 = $buffer.Length
                    $output = $response.OutputStream
                    $output.Write($buffer,0,$buffer.Length)
                    $output.Close()
				    $commandOutput = [string]::Empty
				    $buffer = ""
                } #End Internal Try to catch bad route commands that cause an exception
                catch {
                    Write-Warning "Route Handler Failed with error. Continuing as if nothing happened. Nothing to see here. $($_.Exception)"
                }   
            }  #End While
        } #End External Try 
        finally {
            #Kill the listener on Ctrl-C
            Write-Host "Stopping Listener"
            $listener.Stop()
        }
     

    }
}