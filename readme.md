## Synopsis

This is simple somewhat-restful, HTTP listener module for powershell. Other PowerShell Web servers had been created for the purpose of running any command, or hosting content, but that is not what I wanted, I wanted something more akin to flask with code executing in various pre-defined routes, easily changed, and manipulated. It has evolved since, supporting multiple ways to post and query data as well as static file hosting. This is a side-project, to fit a need that I had. It is far from complete, but I wanted to share in case anyone else was looking for what I was.

## Getting your server started

**Activie Directory Module is required by default, but can be commented out of the psd1 if you only intend on anonymous authentication**

New-PscorpionServer -Name MyTestServer -Port 9090  -Auth IntegratedWindowsAuthentication

Start-Pscorpion

*This will run the server in the foreground. Start-PscorpionJob will start in the background*

```
C:\> Invoke-RestMethod http://localhost:9090/info -UseDefaultCredentials
^reloadroutes$: Reload Routes on POST
info: Return All Routes
^pages/.+$: get static content
```

## The Files

### Inside the created server directory you will find

Static: Folder for static files

config.xml: server configuration data

ServerName_Routes.ps1: Route file containing all routes and commands available on the server

## The Routes

### The default route file includes some help and examples of what is possible, as well as some default routes:

/pages/ - access to static files

/reloadroutes - remotely reload the route file 

/info - view available routes on the server


### The routes are stored in a hash of hashes, with each route containing the following

    "routename" = @{
    	info = "description"
    	security = "allusers <or> AD_Group_Name"
      init = {}
    	get = {}
    	post = {}
     };

Info is displayed when invoking the /info route
security will limit access to an AD group if necessary, all users will allow any authenticated user
init single run initialization commands run at load time
get/post each can contain a given script to run on that HTTP method



Route Names can be a simple string, or use a regex to validate the URL

   Example: "userid" or "^userid/\d{10}$

Match groupings are also supported such as the route: "^route/(?<user>[A-Z0-9.-]+)/(?<prop>[A-Z0-9.-]+)/?$" = @{

   Example: /route/bob.smith/FullName  would give $Matches.user = bob.smith and $matches.prop = FullName

### Data Available within these routes

 $data: a hash of all query string Data, accessible as $data["key"]
   Example: /route?id=1234&user=bob
   accessible as $data['id'] and $data['user']

 $SubUrl: Remainder of route after the initial route
   Example: /route/web/page.html
   $SubUrl = web/page.html

 $UrlParts: Array of URL elements
   Example: /route/bob/profile:
   $UrlParts = @(route, bob, profile); $urlparts[1] = bob